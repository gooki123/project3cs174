package cs174project3;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.imageio.*;


public class MainFrame extends JFrame implements ActionListener{

    private PatientPanel patientPanel;
    private DoctorPanel doctorPanel;
    private AdminPanel adminPanel;
    
    private MenuPanel menuPanel;
   
    public MainFrame(){
		

		patientPanel = new PatientPanel();
		patientPanel.getHomeButton().addActionListener(this);
		
		doctorPanel = new DoctorPanel();
		doctorPanel.getHomeButton().addActionListener(this);
		
		adminPanel = new AdminPanel();
		adminPanel.getHomeButton().addActionListener(this);
		
		menuPanel = new MenuPanel();
		menuPanel.getPatientButton().addActionListener(this);
		menuPanel.getDoctorButton().addActionListener(this);
		menuPanel.getAdminButton().addActionListener(this);	
			
		this.setSize(980, 680);
		this.setTitle("Health messages exchange interfaace");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.getContentPane().add(patientPanel);
		this.getContentPane().add(doctorPanel);
		this.getContentPane().add(adminPanel);
		this.getContentPane().add(menuPanel);
		this.setVisible(true);
    }
	
    @Override
    public void actionPerformed(ActionEvent e){
		if(e.getActionCommand().matches("Patient")){
			menuPanel.setVisible(false);
			patientPanel.setVisible(true);
		}
		else if(e.getActionCommand().matches("Doctor")) {
			menuPanel.setVisible(false);
			doctorPanel.setVisible(true);
		}
		else if(e.getActionCommand().matches("Administrator")) {
			menuPanel.setVisible(false);
			adminPanel.setVisible(true);
		}
		else if(e.getSource() == patientPanel.getHomeButton()) {
			menuPanel.setVisible(true);
			patientPanel.setVisible(false);
			patientPanel.initSet();
			
		}
		else if(e.getSource() == doctorPanel.getHomeButton()) {
			menuPanel.setVisible(true);
			doctorPanel.setVisible(false);
			doctorPanel.initSet();
		}
		else if(e.getSource() == adminPanel.getHomeButton()) {
			menuPanel.setVisible(true);
			adminPanel.setVisible(false);
			adminPanel.initSet();
		}
		

    }

}
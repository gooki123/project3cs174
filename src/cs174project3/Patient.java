package cs174project3;

public class Patient {
  private String patientid;
  private String patientrole;
  private String givenname;
  private String familyname;
  private String suffix;
  private String gender;
  private String birthtime;
  private String providerId;
  private String purpose;
  private String xmlCreationdate;
  private String payerid;
	
public Patient(String ID, 
		String givenname, 
		String familyname, 
		String suffix, 
		String gender, 
		String birthtime, 
		String providerid, 
		String purpose, 
		String xmlCreationDate,
		String payerid,
		String patientrole)
{
  this.patientid = ID;
  this.patientrole = patientrole;
  this.givenname = givenname;
  this.familyname = familyname;
  this.suffix = suffix;
  this.gender = gender;
  this.birthtime = birthtime;
  this.providerId = providerid;
  this.purpose = purpose;
  this.xmlCreationdate = xmlCreationDate;
  this.payerid = payerid;
}
	

  public String insertToPatientTable(){
		String insertingQ = "insert into Patient_Guarded_And_Belongs (PatientID,"
				+ "GivenName,"
				+ "FamilyName,"
				+ "Suffix,"
				+ "Gender,"
				+ "BirthTime,"
				+ "ProviderID,"
				+ "Purpose,"
				+ "xmlHealth_creation_date_time,"
				+ "PayerID,"
				+ "PatientRole)"
				+ "values("
				+"\"" + this.patientid +"\"" +","
				+"\""+this.givenname+"\""+","
				+"\""+this.familyname+"\""+","
				+"\""+this.suffix+"\""+","
				+"\""+this.gender+"\""+","
				+"\""+this.birthtime+"\""+","
				+"\""+this.providerId+"\""+","
				+"\""+this.purpose+"\""+","
				+"\""+this.xmlCreationdate+"\""+","
				+"\""+this.payerid+"\""+","
				+"\""+this.patientrole+"\""+");";
		return insertingQ;
  }
	public boolean isnotNUll(){
		return !(patientid == null || patientrole == null || payerid == null);
	}
}

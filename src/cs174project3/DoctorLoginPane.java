package cs174project3;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class DoctorLoginPane extends JPanel {

	
	public JTextField authorIdField;

	public JLabel authorIdLabel;

	public JButton loginButton;
	
	public DoctorLoginPane(){
		setLayout(null);
		setBounds(220, 60, 550, 450);
    	setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
    	init();
	}
	 
	public void init(){

		authorIdLabel = new JLabel("Patient Id");
        authorIdLabel.setBounds(140, 17, 100, 20);
        authorIdLabel.setForeground(Color.black);
        authorIdLabel.setToolTipText("Patient Id");
        add(authorIdLabel);

        authorIdField = new JTextField();
        authorIdField.setBounds(240, 20, 150, 20);
        add(authorIdField);

        
        loginButton = new JButton("View Info");
        loginButton.setBounds(240, 50, 150, 20);
        loginButton.setForeground(Color.black);
        add(loginButton);

	}


}

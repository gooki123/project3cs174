package cs174project3;

import java.sql.SQLException;

public class Author {
	private String authorid;
	private String participatingrole;
	private String authortitle;
	private String authorfirstname;
	private String authorlastname;

	  
	public Author(String authorid,
			String participatingrole, 
			String authortitle,
			String authorfirstname,
			String authorlastname){
	  this.authorid = authorid;
	  this.participatingrole = participatingrole;
	  this.authorfirstname = authorfirstname;
	  this.authortitle = authortitle;
	  this.authorlastname = authorlastname;


	}
	 public String insertToAuthorTable() throws SQLException{
			String insertingQ = "insert into Author (AuthorID,"
					+ "ParticipatingRole,"
					+ "AuthorTitle,"
					+ "AuthorFirstName,"
					+ "AuthorLastName)"
					+ "values("
					+"\"" + this.authorid +"\"" +","
					+"\""+this.participatingrole+"\""+","
					+"\""+this.authortitle+"\""+","
					+"\""+this.authorfirstname+"\""+","
					+"\""+this.authorlastname+"\""+");";
			return insertingQ;
	 }
	 
	 public boolean isnotNUll(){
			return !(authorid == null);
	 }
}

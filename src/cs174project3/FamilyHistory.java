package cs174project3;

import java.sql.SQLException;

public class FamilyHistory {
	
	private String id;
	private String age;
	private String diagnosis;

	
	public FamilyHistory(
			String id,
			String age,
			String diagnosis){
		this.id = id;
		this.age = age;
		this.diagnosis = diagnosis;
	}
	public String insertToFamilyHistoryTable() throws SQLException{
		String insertingQ = "insert into FamilyHistory ("
				+ "Id,"
				+ "Age,"
				+ "Diagnosis)"
				+ "values("
				+"\"" + this.id +"\"" +","
				+"\""+this.age+"\""+","
				+"\""+this.diagnosis+"\""+");";
		return insertingQ;
	}
	
	public boolean isnotNUll(){
		return !(id == null);
	}
}

package cs174project3;

import java.sql.SQLException;

public class RecordedBy {
	
	private String id;
	private String patientid;
	private String relationship;

	
	public RecordedBy(
			String id,
			String patientid,
			String relationship){
		this.id = id;
		this.patientid = patientid;
		this.relationship = relationship;
	}
	public String insertToRecordedByTable() throws SQLException{
		String insertingQ = "insert into Recorded_By ("
				+ "Id,"
				+ "PatientID,"
				+ "Relationship)"
				+ "values("
				+"\"" + this.id +"\"" +","
				+"\""+this.patientid+"\""+","
				+"\""+this.relationship+"\""+");";
		return insertingQ;
	}
	public boolean isnotNUll(){
		return !(id == null || patientid==null);
	}
}

package cs174project3;

import java.sql.SQLException;

public class Assigned {
	private String authorid;
	private String patientid;
	
	public Assigned(String authorid,
			String patientid){
	  this.authorid = authorid;
	  this.patientid = patientid;
	 
	}
	 public String insertToAssignedTable() throws SQLException{
			String insertingQ = "insert into Assigned (AuthorID,"
					+ "PatientID)"
					+ "values("
					+"\"" + this.authorid +"\"" +","
					+"\""+this.patientid+"\""+");";
			return insertingQ;
	 }
	 
	public boolean isnotNUll(){
		return !(authorid == null || patientid==null);
	}
}

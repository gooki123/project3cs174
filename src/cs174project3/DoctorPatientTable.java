package cs174project3;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;


public class DoctorPatientTable extends JPanel{
	public static JTable table;
    public JScrollPane pane;
    
    String[] header = {"PatientID", 
    		"PatientRole", 
    		"GivenName", 
    		"FamilyName", 
    		"Suffix",
    		"Gender",
    		"Birthtime",
    		"ProviderID",
    		"xmlHealth"};
    
    public Object[][] object;
    int numberOfPatients;
    String authorid;
    
    public DoctorPatientTable(String authorid, int width, int height){
    	this.authorid = authorid;
    	repaint();
        setSize(980, 570);
        setLayout(null);
        
        numberOfPatients = 200; // read from sql
        
        object = new Object[numberOfPatients][9];
        table = new JTable(numberOfPatients, 9);
        table.setModel(new javax.swing.table.DefaultTableModel(object, header));
        table.setGridColor(new Color(0, 0, 150));
        table.setEnabled(false);
        refreshPatientList();
        
        pane = new JScrollPane(table);
        pane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        pane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        pane.setBounds(5, 5, width, height);
        add(pane);
        repaint();
    }
    
    public static void refreshPatientList() {
//        try {
//            //commit();
//
//            //Statement st = Main.panel.connection.createStatement();
//
//            //ResultSet res = st.executeQuery("SELECT * FROM Patients WHERE authorid = authorid");
//
//            int i = 0;
//            
//            for (int k = 0; k < table.getRowCount(); k++) {
//                for (int j = 0; j < table.getColumnCount(); j++) {
//                    table.setValueAt("", k, j);
//                }
//            }
//            while (res.next()) {
//                
//                
//                table.setValueAt(id, i, 0);
//                table.setValueAt(title, i, 1);
//                table.setValueAt(subject, i, 2);
//                table.setValueAt(author, i, 3);
//                table.setValueAt(state, i, 4);
//
//                i++;
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }

    }
    

    
}

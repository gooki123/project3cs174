package cs174project3;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;


public class GuardianInfoPane extends JPanel implements ActionListener{

	static String url = "jdbc:mysql://localhost:3306/healthmessagesexchange3";
    static String url2 = "jdbc:mysql://localhost:3306/HealthInformationSystem";
    static String user = "root";
    static String password = "";
    
    static Connection myConnection;
    static Statement myStatement;
	
	public JTextField firstNameField;
	public JTextField lastNameField;
	public JTextField phoneField;
	public JTextField addressField;
	public JTextField cityField;
	public JTextField stateField;
	public JTextField zipField;
	
	public JLabel firstNameLabel;
	public JLabel lastNameLabel;
	public JLabel phoneLabel;
	public JLabel addressLabel;
	public JLabel cityLabel;
	public JLabel stateLabel;
	public JLabel zipLabel;
	
	public JButton editButton;
	public JButton saveButton;
	
	public GuardianInfoPane(){
		setLayout(null);
		setBounds(220, 60, 550, 450);
    	setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
    	setVisible(false);
    	addInfoContainer();
	}
	 
	public void addInfoContainer(){
		
		try {
			myConnection = DriverManager.getConnection(url2,user,password);
			myStatement = myConnection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		firstNameLabel = new JLabel("First Name");
        firstNameLabel.setBounds(140, 17, 100, 20);
        firstNameLabel.setForeground(Color.black);
        firstNameLabel.setToolTipText("First Name");
        add(firstNameLabel);

        firstNameField = new JTextField();
        firstNameField.setBounds(240, 20, 150, 20);
        firstNameField.setEditable(false);
        add(firstNameField);

        lastNameLabel = new JLabel("Last Name");
        lastNameLabel.setBounds(140, 57, 100, 20);
        lastNameLabel.setForeground(Color.black);
        lastNameLabel.setToolTipText("Last Name");
        add(lastNameLabel);

        lastNameField = new JTextField();
        lastNameField.setBounds(240, 60, 150, 20);
        lastNameField.setEditable(false);
        add(lastNameField);
        
        phoneLabel = new JLabel("Phone");
        phoneLabel.setBounds(140, 97, 100, 20);
        phoneLabel.setForeground(Color.black);
        phoneLabel.setToolTipText("Suffix");
        add(phoneLabel);
        
        phoneField = new JTextField();
        phoneField.setBounds(240, 100, 150, 20);
        phoneField.setEditable(false);
        add(phoneField);
        
        addressLabel = new JLabel("Address");
        addressLabel.setBounds(140, 137, 100, 20);
        addressLabel.setForeground(Color.black);
        addressLabel.setToolTipText("Birth Date");
        add(addressLabel);
        
        addressField = new JTextField();
        addressField.setBounds(240, 140, 150, 20);
        addressField.setEditable(false);
        add(addressField);
        
        cityLabel = new JLabel("City");
        cityLabel.setBounds(140, 177, 100, 20);
        cityLabel.setForeground(Color.black);
        cityLabel.setToolTipText("City");
        add(cityLabel);
        
        cityField = new JTextField();
        cityField.setBounds(240, 180, 150, 20);
        cityField.setEditable(false);
        add(cityField);
        
        stateLabel = new JLabel("State");
        stateLabel.setBounds(140, 217, 100, 20);
        stateLabel.setForeground(Color.black);
        stateLabel.setToolTipText("State");
        add(stateLabel);
        
        stateField = new JTextField();
        stateField.setBounds(240, 220, 150, 20);
        stateField.setEditable(false);
        add(stateField);
        
        zipLabel = new JLabel("Zip");
        zipLabel.setBounds(140, 257, 100, 20);
        zipLabel.setForeground(Color.black);
        zipLabel.setToolTipText("Zip");
        add(zipLabel);
        
        zipField = new JTextField();
        zipField.setBounds(240, 260, 150, 20);
        zipField.setEditable(false);
        add(zipField);
        
        editButton = new JButton("Edit");
        editButton.setBounds(240, 300, 150, 20);
        editButton.setForeground(Color.black);
        add(editButton);
        
        saveButton = new JButton("Save");
        saveButton.setBounds(240, 340, 150, 20);
        saveButton.setForeground(Color.black);
        add(saveButton);
        
        editButton.addActionListener(this);
    	saveButton.addActionListener(this);
	}

	public void editable(){
		firstNameField.setEditable(true);
		lastNameField.setEditable(true);
        phoneField.setEditable(true);
        addressField.setEditable(true);
        cityField.setEditable(true);
        stateField.setEditable(true);
        zipField.setEditable(true);
	}
	
	public void noteditable(){
		firstNameField.setEditable(false);
		lastNameField.setEditable(false);
        phoneField.setEditable(false);
        addressField.setEditable(false);
        cityField.setEditable(false);
        stateField.setEditable(false);
        zipField.setEditable(false);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == editButton){
			editable();
		}
		else if(e.getSource() == saveButton){
			if(firstNameField.isEditable()==true){
				String firstName = firstNameField.getText();
				String lastName = lastNameField.getText();
				String phone = phoneField.getText();
				String address = addressField.getText();
				String city = cityField.getText();
				String state = stateField.getText();
				String zip = zipField.getText();
				
				System.out.println(firstName);
				System.out.println(lastName);
				System.out.println(phone);
				System.out.println(address);
				System.out.println(city);
				System.out.println(state);
				System.out.println(zip);
				

				
				try {
					ResultSet ress = myStatement.executeQuery("SELECT * FROM Patient_Guarded_And_Belongs "
							+ "WHERE PatientID = \""+PatientPanel.id+"\";");
					String guardNo="";
					while(ress.next()){
						guardNo = ress.getString("PatientRole");
					}
					
					int res = myStatement.executeUpdate("UPDATE Guardians "
							+ "SET GivenName=\""+firstName+"\", "
									+ "FamilyName=\""+lastName+"\","
									+ "Phone=\""+phone+"\","
									+ "Address=\""+address+"\","
									+ "City=\""+city+"\","
									+ "State=\""+state+"\","
									+ "Zip=\""+zip+"\""
									+ "WHERE GuardianNo=\""+guardNo+"\";");
					System.out.println("hererer" + state);
				}catch (SQLException e1) {
					e1.printStackTrace();}
			}
			else{
				JOptionPane.showMessageDialog(this,
                        "Nothing is changed");
			}
			noteditable();
		}
		
	}

}

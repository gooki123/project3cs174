package cs174project3;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class PatientLoginPane extends JPanel {

	Container loginContainer;
	
	public JTextField patientidField;	
	public JLabel patientidLabel;
	
	String[] displayChoice = new String[] {"Patient Info", "Guardian Info"};
	JComboBox displayChoiceBox;
	public JLabel displayChoiceLabel;
	
	public JButton loginButton;
	
	public PatientLoginPane(){
		setLayout(null);
		setBounds(220, 60, 550, 450);
    	setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
    	
    	addLoginContainer();
	}
	 
	public void addLoginContainer(){
		loginContainer = new Container();
		loginContainer.setSize(540, 438);
		loginContainer.setLayout(null);
		loginContainer.setVisible(true);
		
		patientidLabel = new JLabel("Patient ID");
        patientidLabel.setBounds(140, 17, 150, 20);
        patientidLabel.setForeground(Color.black);
        patientidLabel.setToolTipText("First Name");
        loginContainer.add(patientidLabel);

        patientidField = new JTextField();
        patientidField.setBounds(270, 20, 150, 20);
        loginContainer.add(patientidField);

        displayChoiceLabel = new JLabel("Choice");
        displayChoiceLabel.setBounds(140, 57, 100, 20);
        displayChoiceLabel.setForeground(Color.black);
        displayChoiceLabel.setToolTipText("First Name");
        loginContainer.add(displayChoiceLabel);
        
        displayChoiceBox = new JComboBox(displayChoice);
        displayChoiceBox.setBounds(270, 60, 150, 20);
        loginContainer.add(displayChoiceBox);
        
        loginButton = new JButton("Login");
        loginButton.setBounds(270, 100, 150, 20);
        loginButton.setForeground(Color.black);

        loginContainer.add(loginButton);
        add(loginContainer);
	}


}

package cs174project3;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class AdminLoginPane extends JPanel  {

	
	public JTextField passwordField;

	public JLabel passwordLabel;

	public JButton loginButton;
	
	public AdminLoginPane(){
		setLayout(null);
		setBounds(220, 60, 550, 450);
    	setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
    	init();
	}
	 
	public void init(){

		
		passwordLabel = new JLabel("Password");
        passwordLabel.setBounds(140, 17, 100, 20);
        passwordLabel.setForeground(Color.black);
        passwordLabel.setToolTipText("Password");
        add(passwordLabel);

        passwordField = new JPasswordField();
        passwordField.setBounds(240, 20, 150, 20);
        add(passwordField);

        
        loginButton = new JButton("Login");
        loginButton.setBounds(240, 50, 150, 20);
        loginButton.setForeground(Color.black);


        add(loginButton);

	}

	
}

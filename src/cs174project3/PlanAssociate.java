package cs174project3;

import java.sql.SQLException;

public class PlanAssociate {
	
	private String planid;
	private String activity;
	private String recorddate;
	private String patientid;
	
	public PlanAssociate(
			String planid,
			String activity,
			String recorddate,
			String patientid){
		this.planid = planid;
		this.activity = activity;
		this.recorddate = recorddate;
		this.patientid = patientid;
		
	}
	public String insertToPlanAssociateTable() throws SQLException{
		String insertingQ = "insert into Plan_Associate ("
				+ "PlanID,"
				+ "Activity,"
				+ "RecordedDate,"
				+ "PatientID)"
				+ "values("
				+"\"" + this.planid +"\"" +","
				+"\""+this.activity+"\""+","
				+"\""+this.recorddate+"\""+","
				+"\""+this.patientid+"\""+");";
		return insertingQ;
  }
	public boolean isnotNUll(){
		return !(planid == null || patientid == null);
	}
}

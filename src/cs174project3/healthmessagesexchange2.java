package cs174project3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;



public class healthmessagesexchange2 {
	static String url = "jdbc:mysql://localhost:3306/healthmessagesexchange3";
    static String url2 = "jdbc:mysql://localhost:3306/HealthInformationSystem";
    static String user = "root";
    static String password = "";
    
    static Connection myConnection;
    static Statement myStatement;
    static Connection myConnection2;
    static Statement insertingStatement;
    static ResultSet myResult;
	//just a main function to get all the information from the healthmessagesexchange2 table
	public static void main(String[] agrs) throws SQLException{
		insertion();
		MainFrame frame = new MainFrame();
	}
	
	public static String selectPatient(String first, String last) throws SQLException{
		  String selectQ = "select * from Patient_Guarded_And_Belongs "
		  		+ "where GivenName = "+ "\"" + first + "\""
		  		+ "and FamilyName = "+ "\"" + last + "\"";
		  return selectQ;
	}
	
	public static void insertion() throws SQLException{
		Connection myConnection = DriverManager.getConnection(url,user,password);
		Statement myStatement = myConnection.createStatement();
		Connection myConnection2 = DriverManager.getConnection(url2,user,password);
		Statement insertingStatement = myConnection2.createStatement();
		ResultSet myResult = myStatement.executeQuery("select * from messages2");

		while(myResult.next()){
			String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
			String emptyStr = "";

			Guardian guardian = new Guardian(myResult.getString("GuardianNo"),
					myResult.getString("Zip"),
					myResult.getString("State"),
					myResult.getString("City"),
					myResult.getString("Phone"),
					myResult.getString("Address"),
					myResult.getString("LastName"),
					myResult.getString("FirstName"));

			InsuranceCompany insuranceCompany = new InsuranceCompany(myResult.getString("PayerId"),
					myResult.getString("Name"),
					myResult.getString("PolicyType"));

			Patient myPatient = new Patient(myResult.getString("patientId"),
					myResult.getString("GivenName"),
					myResult.getString("FamilyName"),
					emptyStr,
					emptyStr,
					myResult.getString("BirthTime"),
					myResult.getString("providerId"),
					myResult.getString("Purpose"),
					date,
					myResult.getString("PayerId"),
					myResult.getString("GuardianNo"));

			PlanAssociate plan = new PlanAssociate(myResult.getString("PlanId"),
					myResult.getString("Activity"),
					myResult.getString("ScheduledDate"),
					myResult.getString("patientId"));
			
			FamilyHistory family = new FamilyHistory(myResult.getString("RelativeId"),
					myResult.getString("age"),
					myResult.getString("Diagnosis"));
			
			RecordedBy recordedby = new RecordedBy(myResult.getString("RelativeId"),
					myResult.getString("patientId"),
					myResult.getString("Relation"));

			LabTestsReports labTestsReports = new LabTestsReports(myResult.getString("LabTestResultId"),
					myResult.getString("ReferenceRangeLow"),
					myResult.getString("ReferenceRangeHigh"),
					myResult.getString("TestResultValue"),
					myResult.getString("LabTestType"),
					myResult.getString("PatientVisitId"));

			Take take = new Take(myResult.getString("LabTestResultId"),
					myResult.getString("patientId"),
					myResult.getString("LabTestPerformedDate"));

			Allergies allergies = new Allergies(myResult.getString("id"),
					myResult.getString("Substance"),
					myResult.getString("Reaction"));

			Has has = new Has(myResult.getString("id"),
					myResult.getString("patientId"),
					myResult.getString("Status"));

			Author author = new Author(myResult.getString("AuthorId"),
					myResult.getString("ParticipatingRole"),
					myResult.getString("AuthorTitle"),
					myResult.getString("AuthorFirstName"),
					myResult.getString("AuthorLastName"));

			Assigned assigned = new Assigned(
					myResult.getString("AuthorId"),
					myResult.getString("patientId"));

			String guardianQuery = guardian.insertToGuardiantTable();
			String insuranceQuery = insuranceCompany.insertToInsuranceCompanyTable();
			String patientQuery = myPatient.insertToPatientTable();
			String planQuery = plan.insertToPlanAssociateTable();
			String familyQuery = family.insertToFamilyHistoryTable();
			String recordedByQuery = recordedby.insertToRecordedByTable();
			String labTestsQuery = labTestsReports.insertToLabTestsReportsTable();
			String takeQuery = take.insertToTakeTable();
			String allergiesQuery = allergies.insertToAllergiesTable();
			String hasQuery = has.insertToHasTable();
			String authorQuery = author.insertToAuthorTable();
			String assignedQuery = assigned.insertToAssignedTable();	
			try{
				insertingStatement.executeUpdate(guardianQuery);
			}
			catch(SQLException se){
				System.out.println("Found Null in guardianQuery");
				continue;
			}
			try{
				insertingStatement.executeUpdate(insuranceQuery);
			}
			catch(SQLException se){
				System.out.println("Found Null in insuranceQuery");
				continue;
			}
			try{
				insertingStatement.executeUpdate(patientQuery);
			}
			catch(SQLException se){
				System.out.println("Found Null in patientQuery");
				continue;
			}
			try{
				insertingStatement.executeUpdate(planQuery);
			}
			catch(SQLException se){
				System.out.println("Found Null in planQuery");
				continue;
			}
			try{
				insertingStatement.executeUpdate(familyQuery);
			}
			catch(SQLException se){
				System.out.println("Found Null in familyQuery");
				continue;
			}
			try{
				insertingStatement.executeUpdate(recordedByQuery);
			}
			catch(SQLException se){
				System.out.println("Found Null in recordedByQuery");
				continue;
			}
			try{
				insertingStatement.executeUpdate(labTestsQuery);
			}
			catch(SQLException se){
				System.out.println("Found Null in labTestsQuery");
				continue;
			}
			try{
				insertingStatement.executeUpdate(takeQuery);
			}
			catch(SQLException se){
				System.out.println("Found Null in takeQuery");
				continue;
			}
			try{
				insertingStatement.executeUpdate(allergiesQuery);
			}
			catch(SQLException se){
				System.out.println("Found Null in allergiesQuery");
				continue;
			}
			try{
				insertingStatement.executeUpdate(hasQuery);
			}
			catch(SQLException se){
				System.out.println("Found Null in hasQuery");
				continue;
			}
			try{
				insertingStatement.executeUpdate(authorQuery);  
			}
			catch(SQLException se){
				System.out.println("Found Null in authorQuery");
				continue;
			}
			try{
				insertingStatement.executeUpdate(assignedQuery);  
			}
			catch(SQLException se){
				System.out.println("Found Null in assignedQuery");
				continue;
			}
		}
	}
}

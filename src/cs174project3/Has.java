package cs174project3;

import java.sql.SQLException;

public class Has {
	private String id;
	private String patientid;
	private String status;

	
	public Has(String id,
			String patientid,
			String status){
	  this.id = id;
	  this.patientid = patientid;
	  this.status = status;
	 
	}
	 public String insertToHasTable() throws SQLException{
			String insertingQ = "insert into Has (AllergiesID,"
					+ "PatientID,"
					+ "Status)"
					+ "values("
					+"\""+this.id+"\""+","
					+"\""+this.patientid+"\""+","
					+"\""+this.status+"\""+");";
			return insertingQ;
	  }
	 
	 public boolean isnotNUll(){
			return !(id == null || patientid == null);
	 }
}

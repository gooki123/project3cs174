package cs174project3;

import java.sql.SQLException;

public class InsuranceCompany {
	
	private String payerid;
	private String name;
	private String policytype;	
	
	public InsuranceCompany(String payerid,String name, String policytype){
		this.payerid = payerid;
		this.name = name;
		this.policytype = policytype;
	}

	public String insertToInsuranceCompanyTable()
			throws SQLException {
		String insertingQ = "insert into Insurance_Company ("
				+ "PayerID," + "Name," + "PolicyType)"
				+ "values(" + "\"" + this.payerid + "\"" + ","
				+ "\"" + this.name + "\"" + "," + "\"" + this.policytype + "\"" + ");";
		return insertingQ;
	}
	public boolean isnotNUll(){
		return !(payerid == null);
	}
}

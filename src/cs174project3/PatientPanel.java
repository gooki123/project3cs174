package cs174project3;
import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class PatientPanel extends JPanel implements ActionListener{

	static String url = "jdbc:mysql://localhost:3306/healthmessagesexchange3";
    static String url2 = "jdbc:mysql://localhost:3306/HealthInformationSystem";
    static String user = "root";
    static String password = "";
    
    static Connection myConnection;
    static Statement myStatement;
	
    static String id;
    private JButton homeButton;
    public PatientLoginPane patientLoginPane; 
    public PatientInfoPane patientInfoPane;
    GuardianInfoPane guardianInfoPane;
    
    public PatientPanel() {
    	
    	try {
			myConnection = DriverManager.getConnection(url2,user,password);
			myStatement = myConnection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	guardianInfoPane = new GuardianInfoPane();
        add(guardianInfoPane);
    	
    	patientInfoPane = new PatientInfoPane();
        add(patientInfoPane);
    	
    	patientLoginPane = new PatientLoginPane();
    	patientLoginPane.loginButton.addActionListener(this);
        add(patientLoginPane);
        
		homeButton = new JButton();
		homeButton.setFont(new Font("TimesRoman",Font.PLAIN,30));
		homeButton.setBounds(25,575,65,65);
		try {
		    Image img = ImageIO.read(getClass().getResource("/homeButton.png"));
		    img = img.getScaledInstance( 50, 50,  java.awt.Image.SCALE_SMOOTH ) ; 
		    homeButton.setIcon(new ImageIcon(img));
		} catch (IOException ex) {}
		
		add(homeButton);
		
		this.setLayout(null);
		this.setSize(980, 680);
		this.setVisible(false);
		this.repaint();
    }
    
    public JButton getHomeButton(){
    	return this.homeButton;
    }
	@Override
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == patientLoginPane.loginButton){
			id = patientLoginPane.patientidField.getText();
            String choiceOption = patientLoginPane.displayChoiceBox.getSelectedItem().toString();

            if (id.equals("")) {
                JOptionPane.showMessageDialog(this,
                        "You didn't complete all fields");
            }
            else if(choiceOption.equals("Patient Info")){
            	try {
					ResultSet res = myStatement.executeQuery("SELECT * FROM Patient_Guarded_And_Belongs WHERE PatientID="+id+";");
					while(res.next()){
						patientInfoPane.firstNameField.setText(res.getString("GivenName"));
						patientInfoPane.lastNameField.setText(res.getString("FamilyName"));
						patientInfoPane.suffixField.setText(res.getString("Suffix"));
						patientInfoPane.genderField.setText(res.getString("Gender"));
						patientInfoPane.birthDateField.setText(res.getString("Birthtime"));						
					}
				}catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();}
           		patientLoginPane.setVisible(false);
           		patientInfoPane.setVisible(true);
            	
            }
            else{
            	try {
					ResultSet res = myStatement.executeQuery("SELECT * FROM Guardians g,Patient_Guarded_And_Belongs p  "
							+ "WHERE g.GuardianNo=p.PatientRole AND p.PatientID = "+id+";");
					while(res.next()){
						guardianInfoPane.firstNameField.setText(res.getString("g.GivenName"));
						guardianInfoPane.lastNameField.setText(res.getString("g.FamilyName"));
						guardianInfoPane.addressField.setText(res.getString("Address"));
						guardianInfoPane.phoneField.setText(res.getString("Phone"));	
						guardianInfoPane.cityField.setText(res.getString("City"));
						guardianInfoPane.stateField.setText(res.getString("State"));
						guardianInfoPane.zipField.setText(res.getString("Zip"));
					}
				}catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();}
           		patientLoginPane.setVisible(false);
           		guardianInfoPane.setVisible(true);
            }
		}
		patientLoginPane.patientidField.setText("");
	}
	
	public void initSet(){
		patientLoginPane.setVisible(true);
		patientInfoPane.setVisible(false);
		guardianInfoPane.setVisible(false);
		guardianInfoPane.firstNameField.setText("");
		guardianInfoPane.lastNameField.setText("");
		guardianInfoPane.addressField.setText("");
		guardianInfoPane.phoneField.setText("");	
		guardianInfoPane.cityField.setText("");
		guardianInfoPane.stateField.setText("");
		guardianInfoPane.zipField.setText("");
		patientInfoPane.firstNameField.setText("");
		patientInfoPane.lastNameField.setText("");
		patientInfoPane.suffixField.setText("");
		patientInfoPane.birthDateField.setText("");		
	}
}
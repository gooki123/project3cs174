package cs174project3;

import java.sql.SQLException;

public class LabTestsReports {
	private String labtestresultid;
	private String referencerangelow;
	private String referencerangehigh;
	private String testresultvalue;
	private String labtesttype;
	private String patientvisitid;

	  
	public LabTestsReports(String labtestresultid,
			String referencerangelow, 
			String referencerangehigh,
			String testresultvalue,
			String labtesttype,
			String patientvisitid){
	  this.labtestresultid = labtestresultid;
	  this.referencerangelow = referencerangelow;
	  this.testresultvalue = testresultvalue;
	  this.referencerangehigh = referencerangehigh;
	  this.labtesttype = labtesttype;
	  this.patientvisitid = patientvisitid;

	}
	 public String insertToLabTestsReportsTable() throws SQLException{
			String insertingQ = "insert into LabTestsReports (LabTestResultId,"
					+ "ReferenceRangeLow,"
					+ "ReferenceRangeHigh,"
					+ "TestResultValue,"
					+ "LabTestType,"
					+ "PatientVisitID)"
					+ "values("
					+"\"" + this.labtestresultid +"\"" +","
					+"\""+this.referencerangelow+"\""+","
					+"\""+this.referencerangehigh+"\""+","
					+"\""+this.testresultvalue+"\""+","
					+"\""+this.labtesttype+"\""+","
					+"\""+this.patientvisitid+"\""+");";
			return insertingQ;
	  }
		public boolean isnotNUll(){
			return !(labtestresultid == null);
		}	 
}

package cs174project3;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.*;


public class MenuPanel extends JPanel{

	
    private JLabel title;
    
    private JButton patientButton;
    private JButton doctorButton;
    private JButton adminButton;
    
    public MenuPanel(){
	
    	this.setLayout(null);
	
		title = new JLabel("Welcome to Health Messages Exchange!",JLabel.CENTER);
		Font titleFont = new Font("TimesRoman",Font.PLAIN,30);
		title.setFont(titleFont);
		title.setBounds(100,50,800,200);
		
			
		patientButton = new JButton("Patient");
		patientButton.setFont(new Font("TimesRoman",Font.PLAIN,30));
		patientButton.setBounds(340,200,300,100);
		
		doctorButton = new JButton("Doctor");
		doctorButton.setFont(new Font("TimesRoman",Font.PLAIN,30));
		doctorButton.setBounds(340,320,300,100);
		
		adminButton = new JButton("Administrator");
		adminButton.setFont(new Font("TimesRoman",Font.PLAIN,30));
		adminButton.setBounds(340,440,300,100);
		
		this.add(title);
		this.add(patientButton);
		this.add(doctorButton);
		this.add(adminButton);
		
		this.setVisible(true);
		this.repaint();
    }
	
    public JButton getPatientButton(){
    	return this.patientButton;
    }
    public JButton getDoctorButton(){
    	return this.doctorButton;
    }
    public JButton getAdminButton(){
    	return this.adminButton;
    }
}
package cs174project3;

import java.sql.SQLException;

public class Take {
	
	private String labtestresultid;
	private String patientid;
	private String labtestperformeddate;

	
	public Take(
			String labtestresultid,
			String patientid,
			String labtestperformeddate){
		this.labtestresultid = labtestresultid;
		this.patientid = patientid;
		this.labtestperformeddate = labtestperformeddate;
	}
	public String insertToTakeTable() throws SQLException{
		String insertingQ = "insert into Take ("
				+ "LabTestResultId,"
				+ "PatientID,"
				+ "LabTestPerformedDate)"
				+ "values("
				+"\"" + this.labtestresultid +"\"" +","
				+"\""+this.patientid+"\""+","
				+"\""+this.labtestperformeddate+"\""+");";
		return insertingQ;
	}
	public boolean isnotNUll(){
		return !(labtestresultid == null || patientid==null);
	}
}

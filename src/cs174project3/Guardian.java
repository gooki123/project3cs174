package cs174project3;

import java.sql.SQLException;

public class Guardian {
	private String guardianno;
	private String zip;
	private String state;
	private String city;
	private String phone;
	private String address;
	private String familyname;
	private String givenname;

	public Guardian(String guardianno,
			String zip, 
			String state,
			String city,
			String phone,
			String address,
			String familyname,
			String givenname){
	  this.guardianno = guardianno;
	  this.zip = zip;
	  this.city = city;
	  this.state = state;
	  this.phone = phone;
	  this.address = address;
	  this.familyname = familyname;
	  this.givenname = givenname;
	}
	 public String insertToGuardiantTable() throws SQLException{
			String insertingQ = "insert into Guardians (GuardianNo,"
					+ "Zip,"
					+ "State,"
					+ "City,"
					+ "Phone,"
					+ "Address,"
					+ "FamilyName,"
					+ "GivenName)"
					+ "values("
					+"\"" + this.guardianno +"\"" +","
					+"\""+this.zip+"\""+","
					+"\""+this.state+"\""+","
					+"\""+this.city+"\""+","
					+"\""+this.phone+"\""+","
					+"\""+this.address+"\""+","
					+"\""+this.familyname+"\""+","
					+"\""+this.givenname+"\""+");";
			return insertingQ;
	 }
	 
	 public boolean isnotNUll(){
			return !(guardianno == null);
	 }
}

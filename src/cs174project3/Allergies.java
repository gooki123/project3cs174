package cs174project3;

import java.sql.SQLException;

public class Allergies {
	
	private String substance;
	private String reaction;
	private String id;

	
	public Allergies(String id,
			String substance,
			String reaction){
		this.id = id;
		this.substance = substance;
		this.reaction = reaction;
	}
	public String insertToAllergiesTable() throws SQLException{
		String insertingQ = "insert into Allergies ("
				+ "AllergiesID,"
				+ "Substance,"
				+ "Reaction)"
				+ "values("
				+"\""+this.id+"\""+","
				+"\""+this.substance+"\""+","
				+"\""+this.reaction+"\""+");";
		return insertingQ;
	}
	
	public boolean isnotNUll(){
		return !(id == null);
	}
	
}

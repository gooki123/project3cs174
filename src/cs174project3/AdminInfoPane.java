package cs174project3;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;


public class AdminInfoPane extends JPanel  implements ActionListener{


	static String url = "jdbc:mysql://localhost:3306/healthmessagesexchange3";
    static String url2 = "jdbc:mysql://localhost:3306/HealthInformationSystem";
    static String user = "root";
    static String password = "";
    
    static Connection myConnection;
    static Statement myStatement;
	
    public static JTable plansTable;
    public JScrollPane plansPane;
    
    String[] authorsHeader = {"AuthorID", 
    		"First Name", 
    		"Last Name"};
    
    public static JTable listTable;
    public JScrollPane listPane;
    
    String[] listHeader = {"AuthorID", 
    		"First Name", 
    		"Last Name"};
    
    public static JTable authorsTable;
    public JScrollPane authorsPane;
    
    String[] allergiesHeader = {"PatientID", 
    		"First Name", 
    		"Last Name"};
    
    
    
    
    public static JTable subsTable;
    public JScrollPane subsPane;
    
    String[] subsHeader = {"Substance", 
    		"Number of patients"};
	
    int numberOfAllergies = 200;
    public Object[][] object;
    
	DoctorPatientTable doctorPatientTable;
	
	JLayeredPane buttonsCont;
	
	JLayeredPane allergiesCont;
	JLayeredPane plansCont;
	JLayeredPane authorsCont;
	JLayeredPane listPatCont;
	
	public JButton allergiesButton;
	public JButton plansButton;
	public JButton authorsButton;
	public JButton listPatientsButton;
	
	
	public AdminInfoPane(){
		
		try {
			myConnection = DriverManager.getConnection(url2,user,password);
			myStatement = myConnection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		
		setLayout(null);
		setBounds(20, 20, 900, 550);
    	setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
    	setVisible(false);
    	init();
    	
    	object = new Object[numberOfAllergies][3];
		plansTable = new JTable(numberOfAllergies, 3);
		plansTable.setModel(new javax.swing.table.DefaultTableModel(object, allergiesHeader));
		plansTable.setGridColor(new Color(0, 0, 150));
		plansTable.setEnabled(false);
        
		plansPane = new JScrollPane(plansTable);
		plansPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		plansPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		plansPane.setBounds(10, 10, 280, 500);
		plansCont.add(plansPane);
		
		listTable = new JTable(numberOfAllergies, 3);
		listTable.setModel(new javax.swing.table.DefaultTableModel(object, allergiesHeader));
		listTable.setGridColor(new Color(0, 0, 150));
		listTable.setEnabled(false);
        
		listPane = new JScrollPane(listTable);
		listPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		listPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		listPane.setBounds(10, 10, 280, 500);
		listPatCont.add(listPane);
		//listPatCont.add(plansPane);
		
		authorsTable = new JTable(numberOfAllergies, 3);
		authorsTable.setModel(new javax.swing.table.DefaultTableModel(object, authorsHeader));
		authorsTable.setGridColor(new Color(0, 0, 150));
		authorsTable.setEnabled(false);
        
		authorsPane = new JScrollPane(authorsTable);
		authorsPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		authorsPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		authorsPane.setBounds(10, 10, 280, 500);
		authorsCont.add(authorsPane);
		
		subsTable = new JTable(numberOfAllergies, 2);
		subsTable.setModel(new javax.swing.table.DefaultTableModel(object, subsHeader));
		subsTable.setGridColor(new Color(0, 0, 150));
		subsTable.setEnabled(false);
        
		subsPane = new JScrollPane(subsTable);
		subsPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		subsPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		subsPane.setBounds(10, 10, 280, 500);
		allergiesCont.add(subsPane);
	}
	 
	public void init(){
		
		listPatCont = new JLayeredPane();
		listPatCont.setBounds(10, 10, 630, 530);
		listPatCont.setBackground(Color.BLUE);
		listPatCont.setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
		listPatCont.setVisible(false);
        add(listPatCont);
		
		authorsCont = new JLayeredPane();
		authorsCont.setBounds(10, 10, 630, 530);
		authorsCont.setBackground(Color.BLUE);
		authorsCont.setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
		authorsCont.setVisible(false);
        add(authorsCont);
		
		plansCont = new JLayeredPane();
		plansCont.setBounds(10, 10, 630, 530);
		plansCont.setBackground(Color.BLUE);
		plansCont.setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
		plansCont.setVisible(false);
        add(plansCont);
		
		allergiesCont = new JLayeredPane();
		allergiesCont.setBounds(10, 10, 630, 530);
		allergiesCont.setBackground(Color.BLUE);
		allergiesCont.setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
		allergiesCont.setVisible(false);
        add(allergiesCont);
		
		
		buttonsCont = new JLayeredPane();
		buttonsCont.setBounds(650, 10, 230, 530);
		buttonsCont.setBackground(Color.BLUE);
		buttonsCont.setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
        add(buttonsCont);
		
		allergiesButton = new JButton("# of Patience by substance");
        allergiesButton.setBounds(35, 60, 180, 20);
        allergiesButton.setForeground(Color.black);
        allergiesButton.addActionListener(this);
        buttonsCont.add(allergiesButton);
        
        plansButton = new JButton("List Plans/Date");
        plansButton.setBounds(55, 100, 120, 20);
        plansButton.setForeground(Color.black);
        plansButton.addActionListener(this);
        buttonsCont.add(plansButton);
        
        authorsButton = new JButton("Authors");
        authorsButton.setBounds(55, 140, 120, 20);
        authorsButton.setForeground(Color.black);
        authorsButton.addActionListener(this);
        buttonsCont.add(authorsButton);   
        
        listPatientsButton= new JButton("List Patients");
        listPatientsButton.setBounds(55, 180, 120, 20);
        listPatientsButton.setForeground(Color.black);
        listPatientsButton.addActionListener(this);
        buttonsCont.add(listPatientsButton);   
        
        
        

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == listPatientsButton){
			System.out.println("here");
			try {

				String sql = "SELECT temp.pid, p.GivenName, p.FamilyName "
						+ "From "
						+ "(SELECT h.PatientID as pid, count(h.AllergiesID) as aid "
						+ "From Has h "
						+ "group by h.PatientID) as temp, Patient_Guarded_And_Belongs p "
						+ "where temp.aid > 1 "
						+ "AND temp.pid = p.PatientID;";
				
				ResultSet res = myStatement.executeQuery(sql);
				
				for (int k = 0; k < listTable.getRowCount(); k++) {
	                for (int j = 0; j <listTable.getColumnCount(); j++) {
	                	listTable.setValueAt("", k, j);
	                }
	            }
				int i = 0;
	            while (res.next()) {
	            	
	            	listTable.setValueAt(res.getString("temp.pid"), i, 0);
	            	listTable.setValueAt(res.getString("p.GivenName"), i, 1);
	            	listTable.setValueAt(res.getString("p.FamilyName"), i, 2);
	                i++;
	            }
				
			}catch (SQLException e1) {
				e1.printStackTrace();}
			
			allergiesCont.setVisible(false);
			plansCont.setVisible(false);
			authorsCont.setVisible(false);
			listPatCont.setVisible(true);
		}
		else if(e.getSource() == allergiesButton){
			
			try {
				String sql =  "select temp.sub, count(temp.pid) "
						+ "from (select distinct a.Substance as sub, h.PatientID as pid From Allergies a, Has h "
						+ "where a.AllergiesID = h.AllergiesID) AS temp "
						+ "where temp.sub<>\"null\" "
						+ "group by temp.sub;";
				System.out.println(sql);
				ResultSet res = myStatement.executeQuery(sql);
				
				for (int k = 0; k < subsTable.getRowCount(); k++) {
	                for (int j = 0; j <subsTable.getColumnCount(); j++) {
	                	subsTable.setValueAt("", k, j);
	                }
	            }
				int i = 0;
	            while (res.next()) {
	            	
	            	subsTable.setValueAt(res.getString("temp.sub"), i, 0);
	            	subsTable.setValueAt(res.getString("count(temp.pid)"), i, 1);
	
	                i++;
	            }
				
			}catch (SQLException e1) {
				e1.printStackTrace();}
			
			allergiesCont.setVisible(true);
			plansCont.setVisible(false);
			authorsCont.setVisible(false);
			listPatCont.setVisible(false);
		}
		else if(e.getSource() == plansButton){
		        JTextField activity = new JTextField();
		        JTextField date  = new JTextField();
		        JPanel panel = new JPanel(new GridLayout(0, 1));
		        panel.add(new JLabel("Activity:"));
		        panel.add(activity);
		        panel.add(new JLabel("Date:"));
		        panel.add(date);
		        int result = JOptionPane.showConfirmDialog(null, panel, "Test",
		            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		        if (result == JOptionPane.OK_OPTION) {
		            String act = activity.getText();
		            String dat = date.getText();
		            
					try {
						String sql =  "SELECT Pa.PatientID,Pa.GivenName, Pa.FamilyName"
	            				+ " FROM Plan_Associate P,Patient_Guarded_And_Belongs Pa "
	            				+ "WHERE P.Activity =\""+act+"\" "
	            						+ "AND Pa.PatientID = P.PatientID"
	            						+ " AND P.RecordedDate = \""+dat+"\";";
						System.out.println(sql);
						ResultSet res = myStatement.executeQuery(sql);
						
						for (int k = 0; k < plansTable.getRowCount(); k++) {
			                for (int j = 0; j <plansTable.getColumnCount(); j++) {
			                	plansTable.setValueAt("", k, j);
			                }
			            }
						int i = 0;
			            while (res.next()) {
			            	
			            	plansTable.setValueAt(res.getString("Pa.PatientID"), i, 0);
			            	plansTable.setValueAt(res.getString("Pa.GivenName"), i, 1);
			            	plansTable.setValueAt(res.getString("Pa.FamilyName"), i, 2);
			
			                i++;
			            }
						
					}catch (SQLException e1) {
						e1.printStackTrace();}
		            allergiesCont.setVisible(false);
					plansCont.setVisible(true);
					authorsCont.setVisible(false); 
					listPatCont.setVisible(false);
		        } 
		        else {
		            System.out.println("Cancelled");
		        }
			
				
			
		}
		else if(e.getSource() == authorsButton){
			try {
				String sql = "SELECT temp.AID, au.AuthorFirstName, au.AuthorLastName "
						+ " From "
						+ "(SELECT a.AuthorID as AID, count(a.PatientID) as PID "
						+ "From Assigned a "
						+ "group by a.AuthorID) as temp, Author au "
						+ "where temp.PID > 1 AND temp.AID = au.AuthorID;";

				ResultSet res = myStatement.executeQuery(sql);
				
				for (int k = 0; k < authorsTable.getRowCount(); k++) {
	                for (int j = 0; j <authorsTable.getColumnCount(); j++) {
	                	authorsTable.setValueAt("", k, j);
	                }
	            }
				int i = 0;
	            while (res.next()) {
	            	
	            	authorsTable.setValueAt(res.getString("temp.AID"), i, 0);
	            	authorsTable.setValueAt(res.getString("au.AuthorFirstName"), i, 1);
	            	authorsTable.setValueAt(res.getString("au.AuthorLastName"), i, 2);
	
	                i++;
	            }
				
			}catch (SQLException e1) {
				e1.printStackTrace();}
					listPatCont.setVisible(false);
					allergiesCont.setVisible(false);
					plansCont.setVisible(false);
					authorsCont.setVisible(true);
		
		}
		
	}

	
}

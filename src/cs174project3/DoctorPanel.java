package cs174project3;
import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DoctorPanel extends JPanel implements ActionListener{

	static String url = "jdbc:mysql://localhost:3306/healthmessagesexchange3";
    static String url2 = "jdbc:mysql://localhost:3306/HealthInformationSystem";
    static String user = "root";
    static String password = "";
    
    static Connection myConnection;
    static Statement myStatement;
   
    static String patientid;
    private JButton homeButton;
    private DoctorLoginPane doctorLoginPane;
    //DoctorPatientTable doctorPatientTable;
    DoctorPatientInfoPane doctorPatientInfoPane;
    public DoctorPanel() {
	
//    	doctorPatientTable = new DoctorPatientTable("0",900,550);
//    	add(doctorPatientTable);
//    	doctorPatientTable.setVisible(false);
    	
    	try {
			myConnection = DriverManager.getConnection(url2,user,password);
			myStatement = myConnection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	doctorPatientInfoPane = new DoctorPatientInfoPane();
    	add(doctorPatientInfoPane);
    	doctorPatientInfoPane.setVisible(false);
    	
    	doctorLoginPane = new DoctorLoginPane();
    	doctorLoginPane.loginButton.addActionListener(this);
        add(doctorLoginPane);
    	
    	homeButton = new JButton();
		homeButton.setFont(new Font("TimesRoman",Font.PLAIN,30));
		homeButton.setBounds(25,575,65,65);
		try {
		    Image img = ImageIO.read(getClass().getResource("/homeButton.png"));
		    img = img.getScaledInstance( 50, 50,  java.awt.Image.SCALE_SMOOTH ) ; 
		    homeButton.setIcon(new ImageIcon(img));
		} catch (IOException ex) {}
		
		
		add(homeButton);
		
		this.setLayout(null);
		this.setSize(980, 680);
		this.setVisible(false);
		this.repaint();
    }
    
    public JButton getHomeButton(){
    	return this.homeButton;
    }
    
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == doctorLoginPane.loginButton){
			patientid = doctorLoginPane.authorIdField.getText();
            
			System.out.println(patientid);
            if (patientid.equals("")) {
                JOptionPane.showMessageDialog(this,
                        "Please enter your id");
            }
            else{
            	boolean authorExist = false;
            	try {
					ResultSet res = myStatement.executeQuery("SELECT * FROM Patient_Guarded_And_Belongs WHERE PatientID="+patientid+";");
					while(res.next()){
						doctorPatientInfoPane.firstNameField.setText(res.getString("GivenName"));
						doctorPatientInfoPane.lastNameField.setText(res.getString("FamilyName"));
						doctorPatientInfoPane.suffixField.setText(res.getString("Suffix"));
						doctorPatientInfoPane.genderField.setText(res.getString("Gender"));
						doctorPatientInfoPane.birthDateField.setText(res.getString("Birthtime"));
						doctorPatientInfoPane.providerIdField.setText(res.getString("ProviderId"));
						doctorPatientInfoPane.xmlField.setText(res.getString("xmlHealth_creation_date_time"));
						doctorPatientInfoPane.patientRoleField.setText(res.getString("PatientRole"));
					}
//					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            	try {
            		String sql= "SELECT * FROM Allergies A WHERE A.AllergiesID IN ("
            				+ "SELECT H.AllergiesID "
            				+ "FROM Has H, Patient_Guarded_And_Belongs P "
            				+ "WHERE P.PatientID ="+patientid+" "
            						+ "AND H.PatientID = P.PatientID "
            						+ "AND A.AllergiesID = H.AllergiesID );";
					ResultSet res = myStatement.executeQuery(sql);
					
					
					int i = 0;
		            while (res.next()) {
		            	
		            	doctorPatientInfoPane.table.setValueAt(res.getString("AllergiesID"), i, 0);
		            	doctorPatientInfoPane.table.setValueAt(res.getString("Substance"), i, 1);
		            	doctorPatientInfoPane.table.setValueAt(res.getString("Reaction"), i, 2);
		
		                i++;
		            }
//					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            	
            	
            	try {
            		String sql= "SELECT * FROM Plan_Associate P WHERE P.PatientID ="+patientid+";";
            						
					ResultSet res = myStatement.executeQuery(sql);
					
					
					int i = 0;
		            while (res.next()) {
		            	
		            	doctorPatientInfoPane.pTable.setValueAt(res.getString("PlanID"), i, 0);
		            	doctorPatientInfoPane.pTable.setValueAt(res.getString("Activity"), i, 1);
		            	doctorPatientInfoPane.pTable.setValueAt(res.getString("RecordedDate"), i, 2);
		
		                i++;
		            }
//					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            	
            	doctorPatientInfoPane.setVisible(true);
            	doctorLoginPane.setVisible(false);
            }
		}
		
	}
    
	public void initSet(){
		doctorLoginPane.setVisible(true);
		doctorLoginPane.authorIdField.setText("");
		doctorPatientInfoPane.setVisible(false);
		for (int k = 0; k < doctorPatientInfoPane.pTable.getRowCount(); k++) {
            for (int j = 0; j < doctorPatientInfoPane.pTable.getColumnCount(); j++) {
            	doctorPatientInfoPane.pTable.setValueAt("", k, j);
            }
        }
		for (int k = 0; k < doctorPatientInfoPane.table.getRowCount(); k++) {
            for (int j = 0; j < doctorPatientInfoPane.table.getColumnCount(); j++) {
            	doctorPatientInfoPane.table.setValueAt("", k, j);
            }
        }
		doctorPatientInfoPane.firstNameField.setText("");
		doctorPatientInfoPane.lastNameField.setText("");
		doctorPatientInfoPane.suffixField.setText("");
		doctorPatientInfoPane.genderField.setText("");
		doctorPatientInfoPane.birthDateField.setText("");
		doctorPatientInfoPane.providerIdField.setText("");
		doctorPatientInfoPane.xmlField.setText("");
		doctorPatientInfoPane.patientRoleField.setText("");
	}
	
	public static void refreshAllergies(){
		
	}
}
package cs174project3;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;


public class DoctorPatientInfoPane extends JPanel implements ActionListener{

	static String url = "jdbc:mysql://localhost:3306/healthmessagesexchange3";
    static String url2 = "jdbc:mysql://localhost:3306/HealthInformationSystem";
    static String user = "root";
    static String password = "";
    
    static Connection myConnection;
    static Statement myStatement;
    
	
	public static JTable table;
    public JScrollPane pane;
    
    public static JTable pTable;
    public JScrollPane pPane;
    
    String[] allergiesHeader = {"AllergyID", 
    		"Substance", 
    		"Reaction"};
	
    String[] planHeader = {"PlanID", 
    		"Activity", 
    		"Date"};
	
    
	int numberOfAllergies = 200;
    public Object[][] object;
    
    
    
	public JTextField firstNameField;
	public JTextField lastNameField;
	public JTextField suffixField;
	public JTextField genderField;
	public JTextField birthDateField;
	public JTextField providerIdField;
	public JTextField xmlField;
	public JTextField patientRoleField;
	
	public JTextArea allergiesField;
	public JTextArea plansField;
	
	public JLabel firstNameLabel;
	public JLabel lastNameLabel;
	public JLabel suffixLabel;
	public JLabel genderLabel;
	public JLabel birthDateLabel;
	public JLabel providerIdLabel;
	public JLabel xmlLabel;
	public JLabel patientRoleLabel;
	
	public JLabel allergiesLabel;
	public JLabel plansLabel;

	public JButton allergyEditButton;
	public JButton plansButton;
	
	public DoctorPatientInfoPane(){
		
		try {
			myConnection = DriverManager.getConnection(url2,user,password);
			myStatement = myConnection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		
		setLayout(null);
		setBounds(100, 60, 750, 450);
    	setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
    	setVisible(false);
    	addInfoContainer();
    	
    	object = new Object[numberOfAllergies][3];
        table = new JTable(numberOfAllergies, 3);
        table.setModel(new javax.swing.table.DefaultTableModel(object, allergiesHeader));
        table.setGridColor(new Color(0, 0, 150));
        table.setEnabled(false);
        
        pane = new JScrollPane(table);
        pane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        pane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        pane.setBounds(450, 17, 280, 100);
        add(pane);
        
        
        object = new Object[numberOfAllergies][3];
        pTable = new JTable(numberOfAllergies, 3);
        pTable.setModel(new javax.swing.table.DefaultTableModel(object, planHeader));
        pTable.setGridColor(new Color(0, 0, 150));
        pTable.setEnabled(false);
        pPane = new JScrollPane(pTable);
        pPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        pPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        pPane.setBounds(450, 130, 280, 100);
        add(pPane);
        repaint();

    	
	}
	 
	public void addInfoContainer(){
	
		firstNameLabel = new JLabel("First Name");
        firstNameLabel.setBounds(40, 17, 100, 20);
        firstNameLabel.setForeground(Color.black);
        firstNameLabel.setToolTipText("First Name");
        add(firstNameLabel);

        firstNameField = new JTextField();
        firstNameField.setBounds(140, 20, 200, 20);
        firstNameField.setEditable(false);
        add(firstNameField);

        lastNameLabel = new JLabel("Last Name");
        lastNameLabel.setBounds(40, 57, 100, 20);
        lastNameLabel.setForeground(Color.black);
        lastNameLabel.setToolTipText("Last Name");
        add(lastNameLabel);

        lastNameField = new JTextField();
        lastNameField.setBounds(140, 60, 200, 20);
        lastNameField.setEditable(false);
        add(lastNameField);
        
        suffixLabel = new JLabel("Suffix");
        suffixLabel.setBounds(40, 97, 100, 20);
        suffixLabel.setForeground(Color.black);
        suffixLabel.setToolTipText("Suffix");
        add(suffixLabel);
        
        suffixField = new JTextField();
        suffixField.setBounds(140, 100, 200, 20);
        suffixField.setEditable(false);
        add(suffixField);
        
        genderLabel = new JLabel("Gender");
        genderLabel.setBounds(40, 137, 100, 20);
        genderLabel.setForeground(Color.black);
        genderLabel.setToolTipText("Gender");
        add(genderLabel);
        
        genderField = new JTextField();
        genderField.setBounds(140, 140, 200, 20);
        genderField.setEnabled(false);
        add(genderField);
        
        birthDateLabel = new JLabel("Birth Date");
        birthDateLabel.setBounds(40, 177, 100, 20);
        birthDateLabel.setForeground(Color.black);
        birthDateLabel.setToolTipText("Birth Date");
        add(birthDateLabel);
        
        birthDateField = new JTextField();
        birthDateField.setBounds(140, 180, 200, 20);
        birthDateField.setEditable(false);
        add(birthDateField);
        
        providerIdLabel = new JLabel("Provider ID");
        providerIdLabel.setBounds(40, 217, 100, 20);
        providerIdLabel.setForeground(Color.black);
        providerIdLabel.setToolTipText("ProviderId");
        add(providerIdLabel);
        
        providerIdField = new JTextField();
        providerIdField.setBounds(140, 220, 200, 20);
        providerIdField.setEditable(false);
        add(providerIdField);
        
        xmlLabel = new JLabel("XML");
        xmlLabel.setBounds(40, 257, 100, 20);
        xmlLabel.setForeground(Color.black);
        xmlLabel.setToolTipText("ProviderId");
        add(xmlLabel);
        
        xmlField = new JTextField();
        xmlField.setBounds(140, 260, 200, 20);
        xmlField.setEditable(false);
        add(xmlField);
        
        patientRoleLabel = new JLabel("Patient Role");
        patientRoleLabel.setBounds(40, 297, 100, 20);
        patientRoleLabel.setForeground(Color.black);
        patientRoleLabel.setToolTipText("ProviderId");
        add(patientRoleLabel);
        
        patientRoleField = new JTextField();
        patientRoleField.setBounds(140, 300, 200, 20);
        patientRoleField.setEditable(false);
        add(patientRoleField);
        
        allergiesLabel = new JLabel("Allergies");
        allergiesLabel.setBounds(350, 17, 100, 20);
        allergiesLabel.setForeground(Color.black);
        allergiesLabel.setToolTipText("allergies");
        add(allergiesLabel);
        
        plansLabel = new JLabel("Plans");
        plansLabel.setBounds(350, 130, 100, 20);
        plansLabel.setForeground(Color.black);
        plansLabel.setToolTipText("plans");
        add(plansLabel);
        
        allergyEditButton = new JButton("Edit Allergies");
        allergyEditButton.setBounds(450, 250, 150, 20);
        allergyEditButton.setForeground(Color.black);
        add(allergyEditButton);
        
        plansButton = new JButton("Edit Plans");
        plansButton.setBounds(450, 300, 150, 20);
        plansButton.setForeground(Color.black);
        add(plansButton);

    	allergyEditButton.addActionListener(this);
    	plansButton.addActionListener(this);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == allergyEditButton){
			JTextField allerid = new JTextField();
	        JTextField substance = new JTextField();
	        JTextField reaction  = new JTextField();
	        JPanel panel = new JPanel(new GridLayout(0, 1));
	        panel.add(new JLabel("Allergy ID:"));
	        panel.add(allerid);
	        panel.add(new JLabel("Substance:"));
	        panel.add(substance);
	        panel.add(new JLabel("Reaction:"));
	        panel.add(reaction);
	        int result = JOptionPane.showConfirmDialog(null, panel, "Test",
	            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
	        if (result == JOptionPane.OK_OPTION) {
	            String id = allerid.getText();
	            String sub = substance.getText();
	            String reac = reaction.getText();
	            
				try {
					int res = myStatement.executeUpdate("UPDATE Allergies "
							+ "SET AllergiesID=\""+id+"\", "
									+ "Substance=\""+sub+"\","
									+ "Reaction=\""+reac+"\""
									+ "WHERE AllergiesID=\""+id+"\";");
					
				}catch (SQLException e1) {
					e1.printStackTrace();}
	            
	            refreshAllergiesTable();
	        } 
	        else {
	            System.out.println("Cancelled");
	        }
		}
		else if(e.getSource() == plansButton){
			JTextField planid = new JTextField();
	        JTextField activity = new JTextField();
	        JTextField date  = new JTextField();
	        JPanel panel = new JPanel(new GridLayout(0, 1));
	        panel.add(new JLabel("Plan ID:"));
	        panel.add(planid);
	        panel.add(new JLabel("Activity:"));
	        panel.add(activity);
	        panel.add(new JLabel("Date:"));
	        panel.add(date);
	        int result = JOptionPane.showConfirmDialog(null, panel, "Test",
	            JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
	        if (result == JOptionPane.OK_OPTION) {
	            String id = planid.getText();
	            String act = activity.getText();
	            String dat = date.getText();
	            
				try {
					int res = myStatement.executeUpdate("UPDATE Plan_Associate "
							+ "SET PlanID=\""+id+"\", "
									+ "Activity=\""+act+"\","
									+ "RecordedDate=\""+dat+"\""
									+ "WHERE PlanID=\""+id+"\";");
					
				}catch (SQLException e1) {
					e1.printStackTrace();}
	            
	            refreshPlansTable();
	        } 
	        else {
	            System.out.println("Cancelled");
	        }
		}
		
	}

	
	public void refreshAllergiesTable(){
		try {
    		String sql= "SELECT * FROM Allergies A WHERE A.AllergiesID IN ("
    				+ "SELECT H.AllergiesID "
    				+ "FROM Has H, Patient_Guarded_And_Belongs P "
    				+ "WHERE P.PatientID ="+DoctorPanel.patientid+" "
    						+ "AND H.PatientID = P.PatientID "
    						+ "AND A.AllergiesID = H.AllergiesID );";
			ResultSet res = myStatement.executeQuery(sql);
			
			for (int k = 0; k < table.getRowCount(); k++) {
                for (int j = 0; j < table.getColumnCount(); j++) {
                	table.setValueAt("", k, j);
                }
            }
			int i = 0;
            while (res.next()) {
            	
            	table.setValueAt(res.getString("AllergiesID"), i, 0);
            	table.setValueAt(res.getString("Substance"), i, 1);
            	table.setValueAt(res.getString("Reaction"), i, 2);

                i++;
            }
//			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void refreshPlansTable(){
		try {
    		String sql= "SELECT * FROM Plan_Associate P WHERE P.PatientID ="+DoctorPanel.patientid+";";
    						
			ResultSet res = myStatement.executeQuery(sql);
			
			for (int k = 0; k < pTable.getRowCount(); k++) {
                for (int j = 0; j < pTable.getColumnCount(); j++) {
                	pTable.setValueAt("", k, j);
                }
            }
			int i = 0;
            while (res.next()) {
            	
            	pTable.setValueAt(res.getString("PlanID"), i, 0);
            	pTable.setValueAt(res.getString("Activity"), i, 1);
            	pTable.setValueAt(res.getString("RecordedDate"), i, 2);

                i++;
            }
//			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}

package cs174project3;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;


public class PatientInfoPane extends JPanel implements ActionListener{

	static String url = "jdbc:mysql://localhost:3306/healthmessagesexchange3";
    static String url2 = "jdbc:mysql://localhost:3306/HealthInformationSystem";
    static String user = "root";
    static String password = "";
    
    static Connection myConnection;
    static Statement myStatement;
	
	
	
	
	public JTextField firstNameField;
	public JTextField lastNameField;
	public JTextField suffixField;
	public JTextField genderField;
	public JTextField birthDateField;
	
	public JLabel firstNameLabel;
	public JLabel lastNameLabel;
	public JLabel suffixLabel;
	public JLabel genderLabel;
	public JLabel birthDateLabel;

	public JButton editButton;
	public JButton saveButton;
	
	public PatientInfoPane(){
		setLayout(null);
		setBounds(220, 60, 550, 450);
    	setBorder(BorderFactory.createEtchedBorder(
                Color.BLACK, Color.GRAY));
    	setVisible(false);
    	addInfoContainer();
	}
	 
	public void addInfoContainer(){
		
		try {
			myConnection = DriverManager.getConnection(url2,user,password);
			myStatement = myConnection.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		firstNameLabel = new JLabel("First Name");
        firstNameLabel.setBounds(140, 17, 100, 20);
        firstNameLabel.setForeground(Color.black);
        firstNameLabel.setToolTipText("First Name");
        add(firstNameLabel);

        firstNameField = new JTextField();
        firstNameField.setBounds(240, 20, 230, 20);
        firstNameField.setEditable(false);
        add(firstNameField);

        lastNameLabel = new JLabel("Last Name");
        lastNameLabel.setBounds(140, 57, 100, 20);
        lastNameLabel.setForeground(Color.black);
        lastNameLabel.setToolTipText("Last Name");
        add(lastNameLabel);

        lastNameField = new JTextField();
        lastNameField.setBounds(240, 60, 230, 20);
        lastNameField.setEditable(false);
        add(lastNameField);
        
        suffixLabel = new JLabel("Suffix");
        suffixLabel.setBounds(140, 97, 100, 20);
        suffixLabel.setForeground(Color.black);
        suffixLabel.setToolTipText("Suffix");
        add(suffixLabel);
        
        suffixField = new JTextField();
        suffixField.setBounds(240, 100, 230, 20);
        suffixField.setEditable(false);
        add(suffixField);
        
        genderLabel = new JLabel("Gender");
        genderLabel.setBounds(140, 137, 100, 20);
        genderLabel.setForeground(Color.black);
        genderLabel.setToolTipText("Gender");
        add(genderLabel);
        
        genderField = new JTextField();
        genderField.setBounds(240, 140, 230, 20);
        genderField.setEnabled(false);
        add(genderField);
        
        birthDateLabel = new JLabel("Birth Date");
        birthDateLabel.setBounds(140, 177, 100, 20);
        birthDateLabel.setForeground(Color.black);
        birthDateLabel.setToolTipText("Birth Date");
        add(birthDateLabel);
        
        birthDateField = new JTextField();
        birthDateField.setBounds(240, 180, 230, 20);
        birthDateField.setEditable(false);
        add(birthDateField);
        
        editButton = new JButton("Edit");
        editButton.setBounds(240, 220, 230, 20);
        editButton.setForeground(Color.black);
        add(editButton);
        
        saveButton = new JButton("Save");
        saveButton.setBounds(240, 260, 230, 20);
        saveButton.setForeground(Color.black);
        add(saveButton);

    	editButton.addActionListener(this);
    	saveButton.addActionListener(this);
	}

	public void editable(){
		firstNameField.setEditable(true);
		lastNameField.setEditable(true);
        suffixField.setEditable(true);
        genderField.setEnabled(true);
        birthDateField.setEditable(true);
	}
	public void noteditable(){
		firstNameField.setEditable(false);
		lastNameField.setEditable(false);
        suffixField.setEditable(false);
        genderField.setEnabled(false);
        birthDateField.setEditable(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == editButton){
			editable();
		}
		else if(e.getSource() == saveButton){
			if(firstNameField.isEditable()==true){
				String firstName = firstNameField.getText();
				String lastName = lastNameField.getText();
				String suffix = suffixField.getText();
				String gender = genderField.getText();
				String birth = birthDateField.getText();
				
				System.out.println(firstName);
				System.out.println(lastName);
				System.out.println(suffix);
				System.out.println(gender);
				System.out.println(birth);
				noteditable();
				
				try {
					int res = myStatement.executeUpdate("UPDATE Patient_Guarded_And_Belongs "
							+ "SET GivenName=\""+firstName+"\", "
									+ "FamilyName=\""+lastName+"\","
									+ "Suffix=\""+suffix+"\","
									+ "Gender=\""+gender+"\","
									+ "Birthtime=\""+birth+"\""
									+ "WHERE PatientID=\""+PatientPanel.id+"\";");
					
				}catch (SQLException e1) {
					e1.printStackTrace();}
			}
			else{
				JOptionPane.showMessageDialog(this,
                        "Nothing is changed");
			}
		}
		
	}

}

package cs174project3;
import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;


public class AdminPanel extends JPanel implements ActionListener{


   
    private JButton homeButton;
    AdminLoginPane adminLoginPane;
    AdminInfoPane adminInfoPane;
    
    public AdminPanel() {
		
    	adminInfoPane = new AdminInfoPane();
    	add(adminInfoPane);
    	
    	adminLoginPane = new AdminLoginPane();
        adminLoginPane.loginButton.addActionListener(this);
    	add(adminLoginPane);
    	
    	homeButton = new JButton();
		homeButton.setFont(new Font("TimesRoman",Font.PLAIN,30));
		homeButton.setBounds(25,575,65,65);
		try {
		    Image img = ImageIO.read(getClass().getResource("/homeButton.png"));
		    img = img.getScaledInstance( 50, 50,  java.awt.Image.SCALE_SMOOTH ) ; 
		    homeButton.setIcon(new ImageIcon(img));
		} catch (IOException ex) {}
		
		
		add(homeButton);
		
		this.setLayout(null);
		this.setSize(980, 680);
		this.setVisible(false);
		this.repaint();
    }
    
    public JButton getHomeButton(){
    	return this.homeButton;
    }
    
	public void initSet(){
		adminLoginPane.setVisible(true);
		adminInfoPane.setVisible(false);
		adminInfoPane.allergiesCont.setVisible(false);
		adminInfoPane.plansCont.setVisible(false);
		adminInfoPane.authorsCont.setVisible(false);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == adminLoginPane.loginButton){
			String password = adminLoginPane.passwordField.getText();
            
            System.out.println(password);
            if (password.equals("")) {
                JOptionPane.showMessageDialog(this,
                        "You didn't complete all fields");
            }
            else if(password.equals("1478")){
            	adminLoginPane.setVisible(false);
            	adminInfoPane.setVisible(true);
            	// CHECK IF THERE IS A PATIENT 
            	// IF THERE IS THEN SHOW INFO
            	// IF NOT GIVE A PROMT
            	
            	
            }
            else{
            	System.out.println(password);
        		JOptionPane.showMessageDialog(this,
                        "Password is incorrect.");
        	}
		}
		
	}
}
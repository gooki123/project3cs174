CREATE DATABASE  IF NOT EXISTS `HealthInformationSystem`;
USE `HealthInformationSystem`;

DROP TABLE IF EXISTS `Insurance_Company`;
CREATE TABLE IF NOT EXISTS Insurance_Company(PayerID VARCHAR(100),Name VARCHAR(100), PolicyType VARCHAR(100), PRIMARY KEY(PayerID));

DROP TABLE IF EXISTS `Guardians`;
CREATE TABLE IF NOT EXISTS Guardians(GuardianNo VARCHAR(100), Zip VARCHAR(100), State VARCHAR(100), City VARCHAR(100), Phone VARCHAR(100), Address VARCHAR(100), FamilyName VARCHAR(100), GivenName VARCHAR(100), PRIMARY KEY(GuardianNo));


DROP TABLE IF EXISTS `Patient_Guarded_And_Belongs`;
CREATE TABLE IF NOT EXISTS Patient_Guarded_And_Belongs(PatientID VARCHAR(100), GivenName VARCHAR(100), FamilyName VARCHAR(100), Suffix VARCHAR(100), Gender VARCHAR(100), Birthtime VARCHAR(100), ProviderID VARCHAR(100), Purpose VARCHAR(100), xmlHealth_creation_date_time VARCHAR(100), PayerID VARCHAR(100) NOT NULL, PatientRole VARCHAR(100) NOT NULL, PRIMARY KEY(PatientID), FOREIGN KEY(PatientRole) REFERENCES Guardians(GuardianNO), FOREIGN KEY(PayerID) REFERENCES Insurance_Company(PayerID));

DROP TABLE IF EXISTS `Plan_Associate`;
CREATE TABLE IF NOT EXISTS Plan_Associate(PlanID VARCHAR(100), Activity VARCHAR(100), RecordedDate VARCHAR(100), PatientID VARCHAR(100) NOT NULL, PRIMARY KEY(PlanID), FOREIGN KEY(PatientID) REFERENCES Patient_Guarded_And_Belongs(PatientID));


DROP TABLE IF EXISTS `FamilyHistory`;
CREATE TABLE IF NOT EXISTS FamilyHistory(Id VARCHAR(100), Age VARCHAR(100), Diagnosis VARCHAR(100), PRIMARY KEY(Id));


DROP TABLE IF EXISTS `Recorded_By`;
CREATE TABLE IF NOT EXISTS Recorded_By(Id VARCHAR(100), PatientID VARCHAR(100), Relationship VARCHAR(100), PRIMARY KEY(Id, PatientID), FOREIGN KEY(Id) REFERENCES FamilyHistory(Id), FOREIGN KEY(PatientID) REFERENCES Patient_Guarded_And_Belongs(PatientID));


DROP TABLE IF EXISTS `LabTestsReports`;
CREATE TABLE IF NOT EXISTS LabTestsReports(LabTestResultId VARCHAR(100), ReferenceRangeLow VARCHAR(100), ReferenceRangeHigh VARCHAR(100), TestResultValue VARCHAR(100), LabTestType VARCHAR(100), PatientVisitID VARCHAR(100), PRIMARY KEY (LabTestResultId), UNIQUE(LabTestResultId, PatientVisitID));


DROP TABLE IF EXISTS `Take`;
CREATE TABLE IF NOT EXISTS Take(LabTestResultId VARCHAR(100), PatientID VARCHAR(100), LabTestPerformedDate VARCHAR(100), PRIMARY KEY(LabTestResultId, PatientID),FOREIGN KEY(LabTestResultId) REFERENCES LabTestsReports (LabTestResultId), FOREIGN KEY(PatientID) REFERENCES Patient_Guarded_And_Belongs(PatientID));

DROP TABLE IF EXISTS `Allergies`;
CREATE TABLE IF NOT EXISTS Allergies(AllergiesID VARCHAR(100), Substance VARCHAR(100), Reaction VARCHAR(100), PRIMARY KEY(AllergiesID));

DROP TABLE IF EXISTS `Has`;
CREATE TABLE IF NOT EXISTS Has(AllergiesID VARCHAR(100), PatientID VARCHAR(100), Status VARCHAR(100), PRIMARY KEY(AllergiesID,PatientID), FOREIGN KEY(AllergiesID) REFERENCES Allergies(AllergiesID), FOREIGN KEY(PatientID) REFERENCES Patient_Guarded_And_Belongs(PatientID));

DROP TABLE IF EXISTS `Author`;
CREATE TABLE IF NOT EXISTS Author(AuthorID VARCHAR(100), ParticipatingRole VARCHAR(100), AuthorTitle VARCHAR(100), AuthorFirstName VARCHAR(100), AuthorLastName VARCHAR(100), PRIMARY KEY(AuthorID));

DROP TABLE IF EXISTS `Assigned`;
CREATE TABLE IF NOT EXISTS Assigned(AuthorID VARCHAR(100), PatientID VARCHAR(100), PRIMARY KEY(AuthorID,PatientID), FOREIGN KEY(AuthorID) REFERENCES Author(AuthorID), FOREIGN KEY(PatientID) REFERENCES Patient_Guarded_And_Belongs(PatientID));
